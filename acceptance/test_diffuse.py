#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   Copyright 2009 Tuukka Turto
#
#   This file is part of pyRayTrace.
#
#   pyRayTrace is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   pyRayTrace is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with pyRayTrace.  If not, see <http://www.gnu.org/licenses/>.

from camera import Camera
from camera import GridViewPort
from camera import Vector
from sphere import Sphere
from scene import Scene
from light import Light

def test_ambient_and_diffuse_light_with_sphere():
    camera = Camera()
    viewport = GridViewPort()
    sphere = Sphere()
    light = Light()
    scene = Scene()    
    
    sphere.location = Vector(0.0, 0.0, 0.0)
    sphere.radius = 5
    sphere.colour = (255,  255,  255)
    sphere.diffuse = 0.5
    
    light.location = Vector(6.0,  5.0,  -20.0)
    
    camera.location = Vector(0.0,  0.0,  -20.0)
    viewport.point0 = Vector(7.0, -7.0,  0.0)
    viewport.point1 = Vector(-7.0,  7.0,  0.0)
    viewport.resolution = (255, 255)
    camera.viewport = viewport
    
    scene.objects.append(sphere)
    scene.cameras.append(camera)
    scene.lights.append(light)
    scene.ambient = 0.1
    scene.render_to_file('./acceptance/report/ambient_and_diffuse_light_with_sphere.png')

def test_ambient_and_diffuse_light_with_two_spheres():
    camera = Camera()
    viewport = GridViewPort()
    sphere1 = Sphere()
    sphere2 = Sphere()
    light = Light()
    scene = Scene()    
    
    sphere1.location = Vector(3.0, 3.0, 0.0)
    sphere1.radius = 5
    sphere1.colour = (255,  0,  0)
    sphere1.diffuse = 0.5
    
    sphere2.location = Vector(3.0, 0.0, 0.0)
    sphere2.radius = 5
    sphere2.colour = (0,  255,  0)
    sphere2.diffuse = 0.5    
    
    light.location = Vector(6.0,  5.0,  -10.0)
    
    camera.location = Vector(0.0,  0.0,  -20.0)
    viewport.point0 = Vector(9.0, -9.0,  0.0)
    viewport.point1 = Vector(-9.0,  9.0,  0.0)
    viewport.resolution = (255,  255)
    camera.viewport = viewport
    
    scene.objects.append(sphere1)
    scene.objects.append(sphere2)
    scene.cameras.append(camera)
    scene.lights.append(light)
    scene.ambient = 0.3
    scene.render_to_file('./acceptance/report/ambient_and_diffuse_light_with_two_spheres.png')

def test_multiple_lights_and_spheres():
    camera = Camera()
    viewport = GridViewPort()
    sphere1 = Sphere()
    sphere2 = Sphere()
    sphere3 = Sphere()
    light1 = Light()
    light2 = Light()
    
    scene = Scene()    
    
    sphere1.location = Vector(3.0, 3.0, 0.0)
    sphere1.radius = 5
    sphere1.colour = (255,  0,  0)
    sphere1.diffuse = 0.5
    
    sphere2.location = Vector(3.0, -2.0, -1.0)
    sphere2.radius = 5
    sphere2.colour = (0,  255,  0)
    sphere2.diffuse = 0.5    
    
    sphere3.location = Vector(-5.0, 1.0, 4.0)
    sphere3.radius = 5
    sphere3.colour = (0,  0,  255)
    sphere3.diffuse = 0.5    
    
    light1.location = Vector(6.0,  5.0,  -10.0)
    light2.location = Vector(-11.0,  1.0,  -4.0)
    
    camera.location = Vector(0.0,  0.0, -20.0)
    viewport.point0 = Vector(9.0, -9.0, -1.0)
    viewport.point1 = Vector(-9.0,  9.0, -1.0)
    viewport.resolution = (255,  255)
    camera.viewport = viewport
    
    scene.objects.append(sphere1)
    scene.objects.append(sphere2)
    scene.objects.append(sphere3)
    scene.cameras.append(camera)
    scene.lights.append(light1)
    scene.lights.append(light2)
    scene.ambient = 0.3
    scene.render_to_file('./acceptance/report/multiple_lights_and_spheres.png')
