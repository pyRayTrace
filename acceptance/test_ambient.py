#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   Copyright 2009 Tuukka Turto
#
#   This file is part of pyRayTrace.
#
#   pyRayTrace is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   pyRayTrace is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with pyRayTrace.  If not, see <http://www.gnu.org/licenses/>.

from camera import Camera
from camera import GridViewPort
from camera import Vector
from sphere import Sphere
from scene import Scene

def test_ambient_light_with_sphere():
    camera = Camera()
    viewport = GridViewPort()
    sphere = Sphere()
    scene = Scene()
    
    sphere.location = Vector(0.0, 0.0, 0.0)
    sphere.radius = 5
    sphere.colour = (255,  255,  255)
    
    camera.location = Vector(0.0,  0.0,  -20.0)
    viewport.point0 = Vector(7.0, -7.0,  0.0)
    viewport.point1 = Vector(-7.0,  7.0,  0.0)
    viewport.resolution = (255,  255)
    camera.viewport = viewport
    
    scene.objects.append(sphere)
    scene.cameras.append(camera)
    scene.render_to_file('./acceptance/report/ambient_light_with_sphere.png')
