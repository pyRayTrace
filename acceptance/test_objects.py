#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   Copyright 2009 Tuukka Turto
#
#   This file is part of pyRayTrace.
#
#   pyRayTrace is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   pyRayTrace is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with pyRayTrace.  If not, see <http://www.gnu.org/licenses/>.

from camera import Camera
from camera import GridViewPort
from camera import Vector
from sphere import Sphere
from scene import Scene
from plane import Plane
from light import Light
from triangle import Triangle

def test_plane_and_spheres():
    camera = Camera()
    viewport = GridViewPort()
    sphere1 = Sphere()
    sphere2 = Sphere()
    sphere3 = Sphere()
    plane = Plane()
    light = Light()
    scene = Scene()

    sphere1.location = Vector(0.0, 6.0, 0.0)
    sphere1.radius = 6
    sphere1.colour = (255, 0, 0)
    sphere1.diffuse = 0.5

    sphere2.location = Vector(-6.0, 6.0, 6.0)
    sphere2.radius = 6
    sphere2.colour = (0, 255, 0)
    sphere2.diffuse = 0.5

    sphere3.location = Vector(6.0, 6.0, 6.0)
    sphere3.radius = 6
    sphere3.colour = (0, 0, 255)
    sphere3.diffuse = 0.5

    plane.location = Vector(0.0, 10.0, 0.0)
    plane.normal = Vector(0.0, -1.0, 0.0)
    plane.colour = (128.0,  128.0, 0.0)
    plane.diffuse = 0.8

    light.location = Vector(0.0,  -20.0,  -5.0)

    camera.location = Vector(0.0,  0.0,  -20.0)
    viewport.point0 = Vector(11.0, -7.0,  0.0)
    viewport.point1 = Vector(-11.0,  15.0,  0.0)
    viewport.resolution = (255,  255)
    camera.viewport = viewport

    scene.objects.append(sphere1)
    scene.objects.append(sphere2)
    scene.objects.append(sphere3)
    scene.objects.append(plane)
    scene.cameras.append(camera)
    scene.lights.append(light)
    scene.ambient = 0.3
    scene.render_to_file('./acceptance/report/plane_and_spheres.png')

def test_planes():
    camera = Camera()
    viewport = GridViewPort()
    plane1 = Plane()
    plane2 = Plane()
    plane3 = Plane()
    light = Light()
    scene = Scene()

    plane1.location = Vector(0.0, 10.0, 0.0)
    plane1.normal = Vector(0.0, -1.0, 0.0)
    plane1.colour = (128.0,  128.0, 0.0)
    plane1.diffuse = 0.8

    plane2.location = Vector(0.0,  10.0,  20.0)
    plane2.normal = Vector(-1.0,  0.0,  -1.0).normalize()
    plane2.colour = (0.0,  128.0,  128.0)
    plane2.diffuse = 0.8

    plane3.location = Vector(0.0,  10.0,  20.0)
    plane3.normal = Vector(1.0,  0.0,  -1.0).normalize()
    plane3.colour = (128.0,  0.0,  128.0)
    plane3.diffuse = 0.8

    light.location = Vector(0.0,  5.0,  -5.0)

    camera.location = Vector(0.0,  0.0,  -20.0)
    viewport.point0 = Vector(11.0, -7.0,  0.0)
    viewport.point1 = Vector(-11.0,  15.0,  0.0)
    viewport.resolution = (255,  255)
    camera.viewport = viewport

    scene.objects.append(plane1)
    scene.objects.append(plane2)
    scene.objects.append(plane3)
    scene.cameras.append(camera)
    scene.lights.append(light)
    scene.ambient = 0.3
    scene.render_to_file('./acceptance/report/planes.png')

def test_simple_triangle():
    camera = Camera()
    viewport = GridViewPort()
    triangle = Triangle()
    light = Light()
    scene = Scene()

    triangle.location = (Vector(-10.0, -2.0, 5.0), Vector(5.0, 10.0, 5.0), Vector(10.0, -2.0, 5.0))
    triangle.colour = (255.0,  0.0,  0.0)
    triangle.diffuse = 0.8
    triangle.normal = Vector(0.0,  0.0,  -1.0)
    light.location = Vector(5.0,  5.0,  0.0)

    camera.location = Vector(0.0,  0.0,  -20.0)
    viewport.point0 = Vector(11.0, -7.0,  0.0)
    viewport.point1 = Vector(-11.0,  15.0,  0.0)
    viewport.resolution = (255,  255)
    camera.viewport = viewport

    scene.objects.append(triangle)
    scene.cameras.append(camera)
    scene.lights.append(light)
    scene.ambient = 0.2
    scene.render_to_file('./acceptance/report/simple_triangle.png')
