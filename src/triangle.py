#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   Copyright 2009 Tuukka Turto
#
#   This file is part of pyRayTrace.
#
#   pyRayTrace is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   pyRayTrace is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with pyRayTrace.  If not, see <http://www.gnu.org/licenses/>.

from vector import Vector

class Triangle:

    def __init__(self):
        self.location = (Vector(0.0,  0.0,  0.0),  Vector(0.0,  0.0,  0.0),  Vector(0.0,  0.0,  0.0))
        self.normal = None
        self.colour = (0, 0, 0)
        self.diffuse = 1.0

    def get_closest_intersection(self,  ray):
        """
        Calculates intersection closest to the origin of the ray. If no
        intersection is found,  returns None. Ray is assumed to be length 1
        """
        denominator = - (ray.origin - self.location[0]).dot(self.normal)
        numerator = ray.direction.dot(self.normal)

        if numerator == 0:
            return None

        t = denominator / numerator

        if t < 0:
            return None

        intersection = ray.origin + ray.direction * t

        a = (self.location[1] - self.location[0]).cross(intersection - self.location[0])
        if a.dot(self.normal) <= 0:
            return None

        a = (self.location[2] - self.location[1]).cross(intersection - self.location[1])
        if a.dot(self.normal) <= 0:
            return None

        a = (self.location[0] - self.location[2]).cross(intersection - self.location[2])
        if a.dot(self.normal) <= 0:
            return None

        return intersection

    def get_normal(self,  point):
        """
        Get normal of plane surface at given location
        """
        if self.normal is None:
            self.normal = (self.location[2] - self.location[1]).cross(self.location[0] - self.location[1])
            print "calculated normal"

        return self.normal

    def __str__(self):
        return self.location.__str__() + ":" + self.normal.__str__()
