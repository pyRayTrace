#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   Copyright 2009 Tuukka Turto
#
#   This file is part of pyRayTrace.
#
#   pyRayTrace is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   pyRayTrace is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with pyRayTrace.  If not, see <http://www.gnu.org/licenses/>.

from math import sqrt

class Vector:

    def __init__(self,  x=None,  y=None,  z=None):
        """
        Construct new vector with given 3 item tuple as a content
        """
        if x is None:
            self.vector = (0, 0, 0)
        else:
            if type(x).__name__ == 'tuple':
                self.vector = x
            else:
                self.vector = (x,  y,  z)

    def __add__(A, B):
        """
        Perform addition for pair of vectors
        """
        return Vector(A[0] + B[0],  A[1] + B[1],  A[2] + B[2])

    def __sub__(A, B):
        return Vector(A[0] - B[0],  A[1] - B[1],  A[2] - B[2])

    def __eq__(A, B):
        """
        Perform equality comparison between two vectors or vector and tuple
        """
        if type(B).__name__ == 'instance':
            if B.__class__.__name__ == 'Vector':
                return A.vector == B.vector

        if type(B).__name__ == 'tuple':
            return A.vector == B

        return False

    def __pow__(A, B):
        return A[0]**B +  A[1]**B + A[2]**B

    def __mul__(A, B):
        """
        Perform multiplication between vector and scalar
        """
        return Vector(B * A[0],  B * A[1],  B * A[2])

    def __getitem__(self,  index):
        if not self.vector == None:
            return self.vector[index]
        else:
            return None

    def __str__(self):
        return self.vector.__str__()

    def dot(A,  B):
        """
        Calculate dot product between two vectors
        """
        return A[0] * B[0] + A[1] * B[1] + A[2] * B[2]

    def cross(A,  B):
        """
        Calculate cross product between two vectors
        """
        return Vector(A[1]*B[2] - A[2]*B[1],  A[2]*B[0] - A[0]*B[2],  A[0]*B[1] - A[1]*B[0])

    def normalize(self):
        """
        Calculate normalized vector based on this vector.
        Does not change values of this vector.
        If normalization can not be done, returns None
        """
        vector = Vector(self.vector)
        length = vector.length()

        if not length == 0:
            return Vector(vector[0] / length,  vector[1] / length,  vector[2] / length)
        else:
            return None


    def length(self):
        return sqrt(self.vector[0]**2 + self.vector[1]**2 + self.vector[2]**2)
