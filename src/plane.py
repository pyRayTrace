#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   Copyright 2009 Tuukka Turto
#
#   This file is part of pyRayTrace.
#
#   pyRayTrace is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   pyRayTrace is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with pyRayTrace.  If not, see <http://www.gnu.org/licenses/>.

from vector import Vector

class Plane:
    
    def __init__(self):
        self.location = Vector(0.0,  0.0,  0.0)
        self.normal = Vector(0.0,  0.0,  1.0)
        self.colour = (0, 0, 0)
        self.diffuse = 1.0
    
    def get_closest_intersection(self,  ray):
        """
        Calculates intersection closest to the origin of the ray. If no
        intersection is found,  returns None. Ray is assumed to be length 1
        """
        denominator = self.location.dot(self.normal) - (ray.origin.dot(self.normal))
        nominator = ray.direction.dot(self.normal)
        
        if nominator == 0:
            return None

        t = denominator / nominator
        
        if t > 0:
            return ray.origin + ray.direction * t
        else:
            return None
    
    def get_normal(self,  point):
        """
        Get normal of plane surface at given location        
        """
        return self.normal

    def __str__(self):
        return self.location.__str__() + ":" + self.normal.__str__()
