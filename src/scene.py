#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   Copyright 2009 Tuukka Turto
#
#   This file is part of pyRayTrace.
#
#   pyRayTrace is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   pyRayTrace is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with pyRayTrace.  If not, see <http://www.gnu.org/licenses/>.

from PIL import Image
from camera import Ray
from vector import Vector

class Scene:
    """
    Master class defining 3D-scene with objects, lights and cameras
    """

    def __init__(self):
        self.objects = []
        self.cameras = []
        self.lights = []
        self.background = (0,  0,  0)
        self.ambient = 1.0

        self.camera = None

    def render_point(self,  x,  y):
        """
        Render single point in scene and return value of it
        """
        ray = Ray(self.camera.location,  self.camera.viewport.get_point(x,  y) - self.camera.location)
        ray = ray.normalize()
        colour = (0, 0, 0)
        ambient = (0, 0,  0)
        diffuse = (0,  0,  0)

        result = self.find_closest_intersection(ray)
        if result != None:
            object = result[0]
            intersection = result[1]
        else:
            object = None

        # calculate lighting on the object
        if object != None:
            ambient = self.calculate_ambient(object, intersection)
            diffuse = self.calculate_diffuse(object, intersection)

        # construct colour (int, int, int)
        colour = (int(ambient[0] + diffuse[0]),  int(ambient[1] + diffuse[1]),  int(ambient[2] + diffuse[2]))

        return colour

    def find_closest_intersection(self, ray):
        """
        Find closest intersection with scenery and given ray
        If intersection is found, returns tuple (object, intersection)
        Otherwise None is returned
        """
        intersection = None
        closest_intersection = None
        closest_object = None
        closest_distance = None

        # find out the closest intersecting object and intersection point on it
        for object in self.objects:
            intersection = object.get_closest_intersection(ray)
            if not intersection == None:
                if closest_object == None:
                    closest_object = object
                    closest_intersection = intersection
                    closest_distance = (self.camera.location - intersection).length()
                else:
                    if (self.camera.location - intersection).length() < closest_distance:
                        closest_object = object
                        closest_intersection = intersection
                        closest_distance = (self.camera.location - intersection).length()

        if closest_object != None:
            return (closest_object, closest_intersection)
        else:
            return None

    def calculate_ambient(self,  object,  intersection):
        """
        Calculate ambient part of the lighting for given object and intersection
        """
        ambient = (object.colour[0] * self.ambient,  object.colour[1] * self.ambient,  object.colour[2] * self.ambient)
        return ambient

    def calculate_diffuse(self,  object,  intersection):
        """
        Calculate diffuse part of the lighting for given object and intersection
        """
        normal = object.get_normal(intersection)
        diffuse = (0, 0, 0)
        for light in self.lights:
            shadow_ray = Ray(intersection, light.location - intersection).normalize()
            shadow_ray_length = Vector(light.location[0] - intersection[0], light.location[1] - intersection[1], light.location[2] - intersection[2]).length()
            blocked = False
            for obstacle in self.objects:
                if obstacle != object:
                    hit = obstacle.get_closest_intersection(shadow_ray)
                    if hit != None:
                        collision_ray = Vector(hit[0] - intersection[0], hit[1] - intersection[1], hit[2] - intersection[2])
                        if collision_ray.length() <= shadow_ray_length:
                            blocked = True
                            break

            if not blocked:
                df = normal.dot(shadow_ray.direction)

                if df > 0:
                    diffuse = (diffuse[0] + object.diffuse * df * object.colour[0],  diffuse[1] + object.diffuse * df * object.colour[1],  diffuse[2] + object.diffuse * df * object.colour[2])

        return diffuse

    def render_to_file(self,  file):
        """
        Render the scene and save results directly to a file

        Will overwrite the target file
        """
        self.camera = self.cameras[0]
        image = Image.new("RGB",  (self.camera.viewport.resolution),  "black")
        img_data = image.load()

        for y in range(self.camera.viewport.resolution[1]):
            for x in range(self.camera.viewport.resolution[0]):
                data = self.render_point(x,  y)
                img_data[x,  y] = data

        image.save(file)
