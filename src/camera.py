#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   Copyright 2009 Tuukka Turto
#
#   This file is part of pyRayTrace.
#
#   pyRayTrace is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   pyRayTrace is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with pyRayTrace.  If not, see <http://www.gnu.org/licenses/>.

from vector import Vector

class Camera:
    """
    Class representing camera in 3D-space
    """
    
    def __init__(self):
        self.location = Vector(0, 0, 0)
        self.viewport = None
        
    def get_viewport_grid(self):
        """
        Get list of 3D points defining viewport grid
        """
        return self.viewport.get_viewport_grid()

    def get_viewport_rays(self):
        """
        Get list of rays originating from camera location and going through the grid.
        Rays are length of one.
        """
        points = self.viewport.get_viewport_grid()
        rays = []
        
        for point in points:
            temp_ray = Ray(self.location, point)
            rays.append(temp_ray.normalize())
        
        return rays

class GridViewPort:
    """
    Class representing rectangular viewport
    """
    
    def __init__(self):
        self.point0 = Vector(0, 0, 0)
        self.point1 = Vector(0, 0, 0)
        self.resolution = (640,  480)
        
    def get_viewport_grid(self):
        """
        Get list of 3D points defining viewport grid
        
        Very simplistic approach for now
        """
        delta_x = (self.point1[0] - self.point0[0]) / self.resolution[0]
        delta_y = (self.point1[1] - self.point0[1]) / self.resolution[1]
        
        points = []
        
        for y in range(self.resolution[1]):
            for x in range(self.resolution[0]):
                points.append((self.point0[0] + x * delta_x,  self.point0[1] + y * delta_y,  self.point0[2]))
        
        return points

    def get_point(self,  x,  y):
        """
        Get point representing viewport grid x, y in 3D-space
        """
        delta_x = (self.point1[0] - self.point0[0]) / self.resolution[0]
        delta_y = (self.point1[1] - self.point0[1]) / self.resolution[1]
        return Vector(self.point0[0] + x * delta_x,  self.point0[1] + y * delta_y,  self.point0[2])

class Ray:
    """
    This class represents simple ray that has origin and direction
    """
    
    def __init__(self,  origin = (0, 0, 0),  direction = (0, 0, 0)):
        """
        Initialize a ray
        
        Accepted parameters for both origin and direction are tuple and Vector
        """
        if type(origin).__name__ == 'tuple':
            self.origin = Vector(origin)
        else:
            self.origin = origin
        
        if type(direction).__name__ == 'tuple':
            self.direction = Vector(direction)
        else:
            self.direction = direction

    def __eq__(A,  B):
        """
        Compare two rays for equality
        
        Rays are equal, if they start from the same location and their direction is same
        """
        if not A.origin == B.origin:
            return False
        
        if not A.direction == B.direction:
            return False
        
        return True

    def __str__(self):
        """
        String representation of the ray
        """
        return self.origin.__str__() + ":" + self.direction.__str__()

    def normalize(self):
        """
        Get normalized ray.
        Origin of the ray will be origin of this ray.
        Direction of the ray will be direction of this ray, divided by the length of this ray (thus, length is 1)
        If normalization can not be done, direction of the ray will be None
        Does not change values of this ray.
        """
        return Ray(self.origin,  self.direction.normalize())
