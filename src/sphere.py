#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   Copyright 2009 Tuukka Turto
#
#   This file is part of pyRayTrace.
#
#   pyRayTrace is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   pyRayTrace is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with pyRayTrace.  If not, see <http://www.gnu.org/licenses/>.

from vector import Vector
from math import sqrt

class Sphere:
    """
    Class representing uniform 3D sphere
    """
    
    def __init__(self):
        self.location = Vector(0, 0, 0)
        self.radius = 0
        self.colour = (0,  0,  0)

    def get_closest_intersection(self,  ray):
        """
        Calculates intersection closest to the origin of the ray. If no
        intersection is found,  returns None. Ray is assumed to be length 1
        """
        b = 2 * ((ray.origin - self.location) .dot(ray.direction))
        c = ((ray.origin - self.location) .dot(ray.origin - self.location)) - (self.radius**2)
       
        discriminant = b**2 - 4*c
      
        if discriminant < 0:
            # no match
            return None
             
        
        t0 = (-b + sqrt(discriminant)) / 2
        t1 = (-b - sqrt(discriminant)) / 2
               
        if t0 < 0:
            if t1 <0:
                return None
            else:
                return ray.origin + ray.direction * t1
        else:
            if t0 < t1:
                return ray.origin + ray.direction * t0
            else:
                return ray.origin + ray.direction * t1

    def get_normal(self,  point):
        """
        Get normal of sphere surface at given location
        Normal is unit vector
        """
        normal = point - self.location
        return normal.normalize()
