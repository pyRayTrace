#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   Copyright 2009 Tuukka Turto
#
#   This file is part of pyRayTrace.
#
#   pyRayTrace is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   pyRayTrace is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with pyRayTrace.  If not, see <http://www.gnu.org/licenses/>.

from plane import Plane
from vector import Vector
from camera import Ray

def test_plane_ray_intersection():
    plane = Plane()
    plane.location = Vector(0.0, 0.0, -5.0)
    plane.normal = Vector(0.0, 0.0, 1.0)
    
    ray = Ray()
    ray.origin = Vector(1.0, 1.0, 5.0)
    ray.direction = Vector(0.0, 0.0, -1.0)
    
    intersection = plane.get_closest_intersection(ray)
    assert intersection == Vector(1.0,  1.0,  -5.0)

def test_plane_ray_intersection_from_far():
    plane = Plane()
    plane.location = Vector(0.0, 0.0, -5.0)
    plane.normal = Vector(0.0, 0.0, 1.0)
    
    ray = Ray()
    ray.origin = Vector(1.0, 1.0, 25.0)
    ray.direction = Vector(0.0, 0.0, -1.0)
    
    intersection = plane.get_closest_intersection(ray)
    assert intersection == Vector(1.0,  1.0,  -5.0)
