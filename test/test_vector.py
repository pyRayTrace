#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   Copyright 2009 Tuukka Turto
#
#   This file is part of pyRayTrace.
#
#   pyRayTrace is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   pyRayTrace is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with pyRayTrace.  If not, see <http://www.gnu.org/licenses/>.

from vector import Vector

def test_addition():
    vector1 = Vector(2, 3, 0)
    vector2 = Vector(-3, 4, 2)

    vector3 = vector1 + vector2
    vector4 = Vector(-1,  7,  2)
    assert vector3 == vector4

def test_substraction():
    vector1 = Vector(3, 4, -5)
    vector2 = Vector(1, 2, 3)

    vector3 = vector2 - vector1
    assert vector3 == (-2, -2, 8)

def test_equality():
    vector1 = Vector(5,  5,  5)
    vector2 = Vector((5,  5,  5))
    vector3 = (5,  5,  5)

    assert vector1 == vector2
    assert vector1 == vector3

def test_multiplication_with_scalar():
    vector1 = Vector(2, -5, 2)
    vector2 = Vector(4,  -10,  4)
    assert vector1 * 2 == vector2

def test_pow():
    vector1 = Vector(2,  3,  4)

    assert vector1**2 == 29

def test_getitem():
    vector1 = Vector(2,  5,  12)

    assert vector1[0] == 2
    assert vector1[1] == 5
    assert vector1[2] == 12

def test_normalization():
    vector1 = Vector(0,  2,  0)

    assert vector1.normalize() == Vector(0,  1,  0)

def test_cross_product():
    # a × b = (a2b3 − a3b2) i + (a3b1 − a1b3) j + (a1b2 − a2b1) k = (a2b3 − a3b2, a3b1 − a1b3, a1b2 − a2b1).
    a = Vector(2, 3, 4)
    b = Vector(-4, 2, 4)

    assert a.cross(b) == Vector(4, -24, 16)
