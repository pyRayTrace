#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   Copyright 2009 Tuukka Turto
#
#   This file is part of pyRayTrace.
#
#   pyRayTrace is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   pyRayTrace is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with pyRayTrace.  If not, see <http://www.gnu.org/licenses/>.

from camera import Camera
from camera import GridViewPort
from camera import Ray
from vector import Vector
  
def test_camera_creation():
    """
    Just a simple camera creation test to ensure that testing environment is working
    """
    camera =  Camera()
    assert not (camera is None)

def test_get_viewport_grid():
    """
    Test that a camera can return a list of coordinates defining grid of viewport.
    """
    camera = Camera()
    viewport = GridViewPort()
    
    camera.location = Vector(0, 0, 0)
    viewport.point0 = Vector(10,  10,  10)
    viewport.point1 = Vector(0,  0,  10)
    viewport.resolution = (11,  11)
    camera.viewport = viewport
    
    points = camera.get_viewport_grid()
    assert (10,  10,  10) in points
    assert (0,  0,  10) in points
    assert (5,  2,  10) in points

def test_get_viewport_rays():
    """
    Test that a camera can return a list of rays, originating from camera location and going through
    the grid
    """
    camera = Camera()
    viewport = GridViewPort()
    ray = Ray((0,  0,  0), (0.0, 0.0, 1.0))
    
    camera.location = Vector(0, 0, 0)
    viewport.point0 = Vector(10,  10,  10)
    viewport.point1 = Vector(0,  0,  10)
    viewport.resolution = (11,  11)
    camera.viewport = viewport
    
    rays = camera.get_viewport_rays()
    assert ray in rays

def test_ray_normalization():
    ray = Ray((0, 0, 0), (10, 0, 0))    
    normalized = ray.normalize()
    assert normalized.origin == (0, 0, 0)
    assert normalized.direction == (1.0,  0.0,  0.0)

def test_ray_normalization_off_origo():
    ray = Ray((2,  4,  -2), (0,  5,  0))
    normalized = ray.normalize()
    assert normalized.origin == (2, 4, -2)
    print normalized.direction.vector
    assert normalized.direction == (0.0,  1.0,  0.0)    

def test_ray_equality():
    ray1 = Ray((0, 0, 0), (5,  5,  5))
    ray2 = Ray((0, 0, 0), (5,  5,  5))
    
    assert ray1 == ray2
