#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   Copyright 2009 Tuukka Turto
#
#   This file is part of pyRayTrace.
#
#   pyRayTrace is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   pyRayTrace is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with pyRayTrace.  If not, see <http://www.gnu.org/licenses/>.

from camera import Ray
from sphere import Sphere
from vector import Vector

def test_closest_intersection():
    sphere = Sphere()
    sphere.location = Vector(10,  0,  0)
    sphere.radius = 5
    
    ray = Ray()
    ray.origin = Vector(0,  0,  0)
    ray.direction = Vector(1,  0,  0)
    
    intersection = sphere.get_closest_intersection(ray)
    assert intersection == Vector(5,  0,  0)

def test_glancing_intersection():
    sphere = Sphere()
    sphere.location = Vector(10,  5,  0)
    sphere.radius = 5
    
    ray = Ray()
    ray.origin = Vector(0,  0,  0)
    ray.direction = Vector(1,  0,  0)
    
    intersection = sphere.get_closest_intersection(ray)
    assert intersection == Vector(10,  0,  0)

def test_miss():
    sphere = Sphere()
    sphere.location = Vector(10,  5,  0)
    sphere.radius = 5
    
    ray = Ray()
    ray.origin = Vector(0,  0,  0)
    ray.direction = Vector(-1,  0,  0)
    
    intersection = sphere.get_closest_intersection(ray)
    assert intersection == None

def test_get_normal():
    sphere = Sphere()
    sphere.location = Vector(10,  0,  0)
    sphere.radius = 5
    
    ray = Ray()
    ray.origin = Vector(0,  0,  0)
    ray.direction = Vector(1,  0,  0)
    
    intersection = sphere.get_closest_intersection(ray)
    normal = sphere.get_normal(intersection)
    
    print normal
    assert normal == (-1,  0,  0)
